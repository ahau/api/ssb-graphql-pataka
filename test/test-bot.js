import AhauClient from 'ahau-graphql-client'

const Server = require('scuttle-testbot')
const { promisify: p } = require('util')

const ahauServer = require('ahau-graphql-server')
const fetch = require('node-fetch')

const Main = require('@ssb-graphql/main')
const Profile = require('@ssb-graphql/profile')
const Pataka = require('../')

module.exports = async function (opts = {}) {
  const ssb = Server // eslint-disable-line
    /* @ssb-graphql/main deps */
    .use(require('ssb-blobs'))
    // .use(require('ssb-serve-blobs'))

    /* @ssb-graphql/profile deps */
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))
    .use(require('ssb-profile'))
    /* @ssb-graphql/pataka deps */
    .use(require('ssb-replicate'))
    .use(require('ssb-friends'))
    .use(require('ssb-tribes'))
    .use(require('ssb-settings'))
    .use(require('ssb-recps-guard'))

    .call(null, {
      serveBlobs: {
        port: 6000 + Math.floor(2000 * Math.random())
      },
      recpsGuard: {
        allowedTypes: ['contact']
      },
      ...opts
    })

  const main = Main(ssb, {
    type: typeof opts.loadContext === 'string' ? opts.loadContext : 'pataka'
  })
  const profile = Profile(ssb)
  const pataka = Pataka(ssb, profile.gettersWithCache)

  let context = {}
  if (opts.loadContext) {
    context = await p(main.loadContext)()
  }

  if (opts.apollo === false) return { ssb, context }

  const port = 3000 + Math.random() * 7000 | 0
  const httpServer = await ahauServer({
    schemas: [
      main,
      profile,
      pataka
    ],
    context: {},
    port
  })
  ssb.close.hook((close, [cb]) => {
    httpServer.close()
    close(cb)
  })

  const apollo = new AhauClient(port, { isTesting: true, fetch })

  return {
    ssb,
    apollo,
    context
  }
}
