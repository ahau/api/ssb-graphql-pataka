const { gql } = require('graphql-tag')

module.exports = gql`
  extend type Query {
    """
    Get list of people who used the Pataka invite code (or who are in range of someone who used an invite code!)
    """
    invitedPeople: [Person]

    """
    Get a summary of records
    """
    dataSummary: DataSummary
  }

  type DataSummary {
    storyRecords: Int
    artefactRecords: Int
    profileRecords: Int
    whakapapaRecords: Int
    communityRecords: Int
  }
`
