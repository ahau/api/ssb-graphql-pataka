const ssbResolvers = require('./ssb')

module.exports = function Resolvers (sbot, gettersWithCache) {
  const {
    getInvitedPeople,
    countCommunities,
    countWhakapapaViews,
    countProfiles
  } = ssbResolvers(sbot, gettersWithCache)

  const resolvers = {
    Query: {
      invitedPeople: async () => getInvitedPeople(),
      dataSummary: async () => {
        return {}
      }
    },
    DataSummary: {
      storyRecords: async () => 0,
      artefactRecords: async () => 0,
      profileRecords: async () => countProfiles(),
      whakapapaRecords: async () => countWhakapapaViews(),
      communityRecords: async () => countCommunities()
    }
  }
  return {
    resolvers,
    gettersWithCache: {}
  }
}
