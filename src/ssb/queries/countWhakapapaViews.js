const pull = require('pull-stream')
const { GraphQLError } = require('graphql')

module.exports = function CountWhakapapaViews (sbot) {
  return function countWhakapapaViews (cb) {
    pull(
      sbot.messagesByType({ type: 'whakapapa/view' }),
      pull.filter(m => {
        const { tangles } = m.value.content
        if (!tangles) return false
        if (!tangles.whakapapa) return false
        return tangles.whakapapa.root === null
      }),
      pull.collect((err, msgs) => {
        if (err) cb(new GraphQLError(err))
        cb(null, msgs.length)
      })
    )
  }
}
